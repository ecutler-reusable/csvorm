const fs = require("fs");

class CsvOrm {
    
    constructor(path=null){
        if(path) this.loadFile(path);
    }
    
    loadFile(file){
        
        this.data = new Promise((resolve,reject) => {
            
            fs.readFile(file, (err, data) => {
                if (err) throw err;
                // split into lines.
                const lines = data.toString().split(/\n/).map(el => el.trim().split(","));      
                
                // first grab headers
                const headers = lines.shift();
                
                // Then format records into object
                const records = lines.map(el => {
                    const out = {}
                    headers.forEach((header,i) => {
                        out[header] = el[i];
                    })
                    return out;
                });
                // then resolve the records and headers.
                resolve({records,headers});
            });
            
        })
        
        this.data.save = () => {
            return new Promise((resolve,reject) => {
                this.data.then(({records,headers}) => {
                    
                    records = records.filter(el => el);
                    
                    // transform records back into lines using headers
                    const lines = records.map(el => {
                        const parts = [];
                        headers.forEach(header => {
                            parts.push(el[header])
                        })
                        return parts.join(",");
                    });
                    
                    // add the headers back to the top.
                    lines.unshift(headers.join(","));
                    
                    // create the full filedata from lines.
                    const fileData = lines.join("\r\n");
                    
                    // write it back to the records.
                    fs.writeFile(file,fileData,err => {
                        if (err) throw err;
                        resolve();
                    })
                });
            })
        }
        
        return this.data;
        
    }
    
}

module.exports = CsvOrm;